<?php
namespace App;
use Slim\Http\Request;
use Slim\Http\Response;
use ShowMapper,ShowEntity;
class ApiController
{
    protected $container;
   // constructor receives container instance
    public function __construct($container) {
       $this->container = $container;
    }
    public function root(Request $request, Response $response) {
        return $response->withJson("Root API Page"); 
    }
    public function showList(Request $request, Response $response) {
        $this->container->logger->addInfo("Shows list");
        $mapper = new ShowMapper($this->container->db);
        $shows = $mapper->getShows();
        return $response->withJson($shows); 
    }
   public function showById($request, $response, $args) {
        $show_id = (int)$args['id'];
       $this->container->logger->addInfo("Fetch Show:$show_id");
        $mapper = new ShowMapper($this->container->db);
        $show = $mapper->getShowById($show_id);
        return $response->withJson($show);
   }
    public function showCreate($request, $response, $args){
    $data = $request->getParsedBody();
    $show_data = [];
    $show_data['title'] = filter_var($data['title'], FILTER_SANITIZE_STRING);
    $show_data['image'] = filter_var($data['image'], FILTER_SANITIZE_URL);
    $show_data['link'] = filter_var($data['link'], FILTER_SANITIZE_URL);
    $show_data['description'] = filter_var($data['description'], FILTER_SANITIZE_STRING);
    $show_data['genre'] = filter_var($data['genre'], FILTER_SANITIZE_STRING);
    $show_data['cast'] = filter_var($data['cast'], FILTER_SANITIZE_STRING);
    $show_data['year'] = filter_var($data['year'], FILTER_SANITIZE_NUMBER_INT);
    $show_data['rating'] = filter_var($data['rating'], FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
    $show = new ShowEntity($show_data);
    $show_mapper = new ShowMapper($this->container->db);
    $show_mapper->save($show);
    $response = $response->getBody()->write("Added Show Successfully");
    return $response;
    }
}
