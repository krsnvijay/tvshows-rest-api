<?php
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

$app->group('/api', function () {
    $this->get('', 'ApiController:root'); 
    $this->group('/shows', function () {
        $this->get('', 'ApiController:showList'); 
        $this->post('', 'ApiController:showCreate');
        $this->group('/{id:[0-9]+}', function () {
            $this->get('','ApiController:showById');    
        }); 
    });
});
