CREATE TABLE `tv-shows-db`.`shows` ( 
    `id` INT(10) NOT NULL AUTO_INCREMENT ,
    `title` VARCHAR(100) NOT NULL ,
    `image` TEXT NOT NULL , 
    `link` TEXT NOT NULL ,
    `description` TEXT NOT NULL , 
    `genre` VARCHAR(50) NOT NULL ,
    `cast` TEXT NOT NULL ,
    `year` YEAR NOT NULL , 
    `rating` FLOAT NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

INSERT INTO `shows` (`id`, `title`, `image`, `link`, `description`, `genre`, `cast`, `year`, `rating`) VALUES
(1, 'Patriot', 'https://images-na.ssl-images-amazon.com/images/M/MV5BNDgwNjI5MzY3Ml5BMl5BanBnXkFtZTgwNDE5NTAzMTI@._V1_SX300.jpg', 'http://www.imdb.com/title/tt4687882/', 'To prevent Iran from going nuclear, intelligence officer John Tavner must forgo all safety nets and assume a perilous \'non-official cover\' -- that of a mid-level employee at a Midwestern industrial piping firm', 'Comedy, Drama, Thriller', 'Michael Dorman, Kurtwood Smith, Michael Chernus, Kathleen Munroe', 2015, 8.3),
(2, 'Mindhunter', 'https://images-na.ssl-images-amazon.com/images/M/MV5BMzNkZmNmZjMtZWI5OC00MzdiLTgxMjAtZWY2ZTIwMWM2Yzc2XkEyXkFqcGdeQXVyMjM5NDQzNTk@._V1_SX300.jpg', 'http://www.imdb.com/title/tt5290382/', 'In the late 1970s two FBI agents expand criminal science by delving into the psychology of murder and getting uneasily close to all-too-real monsters.', 'Crime, Drama, Thriller', 'Jonathan Groff, Holt McCallany, Hannah Gross, Sonny Valicenti', 2017, 8.6),
(3, 'The Americans', 'https://images-na.ssl-images-amazon.com/images/M/MV5BMTEzODU2ODY5ODdeQTJeQWpwZ15BbWU4MDAwMDM0NTEy._V1_SX300.jpg', 'http://www.imdb.com/title/tt2149175/', 'At the height of the Cold War two Russian agents pose as your average American couple, complete with family.', 'Crime, Drama, Mystery', 'Keri Russell, Matthew Rhys, Keidrich Sellati ', 2013, 8.3);

