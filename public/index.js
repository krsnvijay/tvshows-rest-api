fetch('http://localhost/tvshows-rest-api/api/shows')
    .then(resp => resp.json()) // Transform the data into json
    .then(data => {
        let container = document.getElementById("container");
        container.innerHTML = "";
        data.forEach(show => {

            let div = document.createElement("div");
            div.classList.add("card");
            div.innerHTML = `
            <img src=${show.image} alt=${show.title}>
            <div class="card-content">
                <div class="card-header">
                    <h1>${show.title}-(${show.year})</h1>
                    <h3>${show.genre}</h3>
                </div>
                  <div class="card-body">
                    <p><strong>Cast</strong>: ${show.cast}</p>
                    <p>${show.description}</p>
                  <a href=${show.link}>More Details</a>
                </div>
            </div>`;
            container.appendChild(div);
        });
    });
