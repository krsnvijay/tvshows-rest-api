<?php
class ShowMapper extends Mapper
{
    public function getShows() {
        $sql = "SELECT id, title, image, link, description,
        genre, cast, year, rating from shows";
        $stmt = $this->db->query($sql);
        $results = [];
        while($row = $stmt->fetch()) {
            $results[] = new ShowEntity($row);
        }
        return $results;
    }
    /**
     * Get one Show by its ID
     *
     * @param int $show_id The ID of the show
     * @return ShowEntity  The show
     */
    public function getShowById($show_id) {
        $sql = "SELECT id, title, image, link, description, 
        genre, cast, year, rating from shows where id = :show_id";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute(["show_id" => $show_id]);
        if($result) {
            return new ShowEntity($stmt->fetch());
        }
    }
    public function save(ShowEntity $show) {
        $sql = "insert into shows
            (id, title, image, link, description, genre, cast, year, rating) values
            (:id, :title, :image, :link, :description, :genre, :cast, :year, :rating)";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            "id" => $show->getId(),
            "title" => $show->getTitle(),
            "image" => $show->getImage(),
            "link" => $show->getLink(),
            "description" => $show->getDescription(),
            "genre" => $show->getGenre(),
            "cast" => $show->getCast(),
            "year" => $show->getYear(),
            "rating" => $show->getRating()
        ]);
        if(!$result) {
            throw new Exception("could not save record");
        }
    }
}
