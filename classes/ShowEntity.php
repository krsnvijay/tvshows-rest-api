<?php
class ShowEntity implements JsonSerializable
{
    protected $id;
    protected $title;
    protected $image;
    protected $link;
    protected $description;
    protected $genre;
    protected $cast;
    protected $year;
    protected $rating;
    
    /**
     * Accept an array of data matching properties of this class
     * and create the class
     *
     * @param array $data The data to use to create
     */
    public function __construct(array $data) {
        // no id if we're creating
        if(isset($data['id'])) {
            $this->id = $data['id'];
        }
        $this->title = $data['title'];
        $this->image = $data['image'];
        $this->link = $data['link'];
        $this->description = $data['description'];
        $this->genre = $data['genre'];
        $this->cast = $data['cast'];
        $this->year = $data['year'];
        $this->rating = $data['rating'];
    }
      public function jsonSerialize() {
        return [
            "id" => $this->id,
            "title" => $this->title,
            "image" => $this->image,
            "link" => $this->link,
            "description" => $this->description,
            "genre" => $this->genre,
            "cast" => $this->cast,
            "year" => $this->year,
            "rating" => $this->rating
        ];
    }
    public function getId() {
        return $this->id;
    }
    public function getTitle() {
        return $this->title;
    }
    public function getImage() {
        return $this->image;
    }
    public function getLink() {
        return $this->link;
    }
    public function getDescription() {
        return $this->description;
    }
    public function getShortDescription() {
        return substr($this->description, 0, 20);
    }
     public function getGenre() {
        return $this->genre;
    }
     public function getCast() {
        return $this->cast;
    }
    public function getYear() {
        return $this->year;
    }
      public function getRating() {
        return $this->rating;
    }
}
